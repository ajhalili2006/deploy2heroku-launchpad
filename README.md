# Deploy To Heroku Website
An GitLab repository for hosting successful deployments to Heroku, made with Middleman.

## Remixed me?
Edit the `data/config.yml` file and change the value for key `repository`. Then, enjoy remixing stuff.

## Local development

1. Clone this project.
1. Download dependencies: `bundle install`. If you see an error that bundler is missing, then try to run `gem install bundler`.
1. Run the project: `bundle exec middleman`.
1. Open http://localhost:4567 in your browser and enjoy editing locally.

## What template do you used?
I used GitLab's [Middleman with SSE template](https://gitlab.com/gitlab-org/project-templates/static-site-editor-middleman) for this to work.
