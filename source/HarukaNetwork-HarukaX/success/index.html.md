---
title: Successfully deployed! | Deploy to Heroku Launchpad
description: Say thank you to people who made this possible.
---
# Thank you!
Haruka X is now deployed to Heroku. Congrats for that!

## Next steps
Hop back to [the dashboard and select your newly-deployed app](https://dashboard.heroku.com) and enable the free dyno.
Then check the logs if the logs and watch out for errors.

## Have problems?
* [**Join the @gobotsupport Telegram chat.**](https://t.me/gobotsupport) Akito and the squad are usually online, so we'll hopefully anwer your questions.
* [**Create a new issue in GitLab.**](https://gitlab.com/HarukaNetwork/OSS/HarukaX/issues/new) Select `Having issues in deploying to Heroku?` template and edit as usual.
